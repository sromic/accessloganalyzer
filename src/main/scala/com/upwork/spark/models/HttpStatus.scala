package com.upwork.spark.models

/**
  * Created by sromic on 19/06/16.
  */
sealed abstract class HttpStatus(val status: String)

final case class HttpInfoStatus(override val status: String) extends HttpStatus(status)
final case class HttpSuccessStatus(override val status: String) extends HttpStatus(status)
final case class HttpRedirectStatus(override val status: String) extends HttpStatus(status)
final case class HttpClientErrorStatus(override val status: String) extends HttpStatus(status)
final case class HttpServerErrorStatus(override val status: String) extends HttpStatus(status)